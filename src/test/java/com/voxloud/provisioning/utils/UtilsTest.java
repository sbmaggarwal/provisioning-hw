package com.voxloud.provisioning.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testGetDecodedMapFromFileFragment() {
        // Setup
        final Map<String, String> expectedResult = new HashMap<>();

        expectedResult.put("domain", "sip.anotherdomain.com");
        expectedResult.put("port", "5161");
        expectedResult.put("timeout", "10");

        // Run the test
        final Map<String, String> result = Utils.getDecodedMapFromFileFragment("domain=sip.anotherdomain.com\nport=5161\ntimeout=10");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetDecodedMapFromJSONFragment() throws Exception {
        // Setup
        final Map<String, String> expectedResult = new HashMap<>();

        expectedResult.put("domain", "sip.anotherdomain.com");
        expectedResult.put("port", "5161");
        expectedResult.put("timeout", "10");

        // Run the test
        final Map<String, String> result = Utils.getDecodedMapFromJSONFragment("{\"domain\":\"sip.anotherdomain.com\",\"port\":\"5161\",\"timeout\":10}\"");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test(expected = JsonProcessingException.class)
    public void testGetDecodedMapFromJSONFragment_ThrowsJsonProcessingException() throws Exception {
        // Setup

        // Run the test
        Utils.getDecodedMapFromJSONFragment("overrideFragment");
    }
}
