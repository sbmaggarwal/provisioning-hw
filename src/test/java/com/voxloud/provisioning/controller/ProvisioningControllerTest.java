package com.voxloud.provisioning.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.voxloud.provisioning.service.ProvisioningService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProvisioningControllerTest {

    @Mock
    private ProvisioningService mockProvisioningService;

    private ProvisioningController provisioningControllerUnderTest;

    @Before
    public void setUp() {
        provisioningControllerUnderTest = new ProvisioningController(mockProvisioningService);
    }

    @Test
    public void testGetConfigForDevice() throws Exception {
        // Setup
        Map<String, String> map = new HashMap<>();
        map.put("message", "Device not found!");
        final ResponseEntity<Map<String, String>> expectedResult = new ResponseEntity<>(map, HttpStatus.OK);
        when(mockProvisioningService.getProvisioningFile("macAddress")).thenReturn(map);

        // Run the test
        final ResponseEntity<Map<String, String>> result = provisioningControllerUnderTest.getConfigForDevice("macAddress");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test(expected = JsonProcessingException.class)
    public void testGetConfigForDevice_ProvisioningServiceThrowsJsonProcessingException() throws Exception {
        // Setup
        when(mockProvisioningService.getProvisioningFile("macAddress")).thenThrow(JsonProcessingException.class);

        // Run the test
        provisioningControllerUnderTest.getConfigForDevice("macAddress");
    }
}
