package com.voxloud.provisioning.service;

import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ProvisioningServiceImplTest {

    @Mock
    private DeviceRepository mockDeviceRepository;
    @Mock
    private Environment mockEnvironment;

    private ProvisioningServiceImpl provisioningServiceImplUnderTest;

    @Before
    public void setUp() {
        provisioningServiceImplUnderTest = new ProvisioningServiceImpl(mockDeviceRepository, mockEnvironment);
    }

    @Test
    public void testGetProvisioningFile() throws Exception {
        // Setup
        final Map<String, String> expectedResult = new HashMap<>();

        // Configure DeviceRepository.getOne(...).
        final Device device = new Device();
        device.setMacAddress("macAddress");
        device.setModel(Device.DeviceModel.CONFERENCE);
        device.setOverrideFragment("overrideFragment");
        device.setUsername("username");
        device.setPassword("password");
        // Run the test
        final Map<String, String> result = provisioningServiceImplUnderTest.getProvisioningFile("macAddress");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetProvisioningFile_EnvironmentReturnsNull() throws Exception {
        // Setup
        final Map<String, String> expectedResult = new HashMap<>();

        // Configure DeviceRepository.getOne(...).
        final Device device = new Device();
        device.setMacAddress("macAddress");
        device.setModel(Device.DeviceModel.CONFERENCE);
        device.setOverrideFragment("overrideFragment");
        device.setUsername("username");
        device.setPassword("password");

        // Run the test
        final Map<String, String> result = provisioningServiceImplUnderTest.getProvisioningFile("macAddress");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetProvisioningFile_ThrowsJsonProcessingException() throws Exception {
        // Setup

        // Configure DeviceRepository.getOne(...).
        final Device device = new Device();
        device.setMacAddress("macAddress");
        device.setModel(Device.DeviceModel.CONFERENCE);
        device.setOverrideFragment("overrideFragment");
        device.setUsername("username");
        device.setPassword("password");
        // Run the test
        provisioningServiceImplUnderTest.getProvisioningFile("macAddress");
    }
}
