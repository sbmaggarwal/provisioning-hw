package com.voxloud.provisioning.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeviceTest {

    private Device deviceUnderTest;

    @Before
    public void setUp() {
        deviceUnderTest = new Device();
    }

    @Test
    public void testEquals() {
        // Setup

        // Run the test
        final boolean result = deviceUnderTest.equals("o");

        // Verify the results
        assertFalse(result);
    }

    @Test
    public void testHashCode() {
        // Setup

        // Run the test
        final int result = deviceUnderTest.hashCode();

        // Verify the results
        assertNotEquals(0, result);
    }

    @Test
    public void testToString() {
        // Setup

        // Run the test
        final String result = deviceUnderTest.toString();

        // Verify the results
        assertNotEquals("result", result);
    }

    @Test
    public void testCanEqual() {
        // Setup

        // Run the test
        final boolean result = deviceUnderTest.canEqual("other");

        // Verify the results
        assertFalse(result);
    }
}
