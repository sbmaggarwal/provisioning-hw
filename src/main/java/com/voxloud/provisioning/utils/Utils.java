package com.voxloud.provisioning.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class Utils {

    public static Map<String, String> getDecodedMapFromFileFragment(String overrideFragment) {
        Map<String, String> decodedFragment = new HashMap<>();
        String[] keyValuePairs = overrideFragment.split("\n");
        for (String str : keyValuePairs) {
            String[] keyValuePair = str.split("=");
            decodedFragment.put(keyValuePair[0], keyValuePair[1]);
        }

        return decodedFragment;
    }

    public static Map<String, String> getDecodedMapFromJSONFragment(String overrideFragment) throws JsonProcessingException {
        return new ObjectMapper().readValue(overrideFragment, new TypeReference<Map<String, String>>() {});
    }
}
