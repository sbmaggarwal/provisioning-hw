package com.voxloud.provisioning.service;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Map;

public interface ProvisioningService {

    Map<String, String> getProvisioningFile(String macAddress) throws JsonProcessingException;
}
