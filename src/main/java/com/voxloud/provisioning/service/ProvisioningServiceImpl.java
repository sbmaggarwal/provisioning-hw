package com.voxloud.provisioning.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;
import com.voxloud.provisioning.utils.Utils;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.StreamSupport;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {

    private final DeviceRepository deviceRepository;
    private final Environment environment;

    private static final String PREFIX = "provisioning.";

    public ProvisioningServiceImpl(
            DeviceRepository deviceRepository,
            Environment environment) {
        this.deviceRepository = deviceRepository;
        this.environment = environment;
    }

    @Override
    public Map<String, String> getProvisioningFile(String macAddress) throws JsonProcessingException {
        Optional<Device> device = deviceRepository.findById(macAddress);
        if (!device.isPresent())
            return Collections.emptyMap();
        return getProvisioningFileMapForDevice(device.get());
    }

    private Map<String, String> getProvisioningFileMapForDevice(Device device) throws JsonProcessingException {
        Map<String, String> provisioningFileMap = getDefaultProperties();

        if (device.getOverrideFragment() != null) {
            provisioningFileMap.put("username", device.getUsername());
            provisioningFileMap.put("password", device.getPassword());
            if (device.getModel() == Device.DeviceModel.DESK) {
                provisioningFileMap.putAll(Utils.getDecodedMapFromFileFragment(device.getOverrideFragment()));
            } else if (device.getModel() == Device.DeviceModel.CONFERENCE) {
                provisioningFileMap.putAll(Utils.getDecodedMapFromJSONFragment(device.getOverrideFragment()));
            }
        }
        return provisioningFileMap;
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> getDefaultProperties() {
        Properties props = new Properties();
        MutablePropertySources propSrcs = ((AbstractEnvironment) environment).getPropertySources();
        StreamSupport.stream(propSrcs.spliterator(), false)
                .filter(ps -> ps instanceof EnumerablePropertySource)
                .map(ps -> ((EnumerablePropertySource) ps).getPropertyNames())
                .flatMap(Arrays::stream)
                .forEach(propName -> props.setProperty(propName, environment.getProperty(propName)));
        Map<String, String> step2 = (Map<String, String>) (Map) props;
        HashMap<String, String> defaultProps = new HashMap<>(step2);
        defaultProps.entrySet().removeIf(entry -> !entry.getKey().startsWith(PREFIX));
        Map<String, String> finalMap = new HashMap<>();
        defaultProps.forEach((k, v) -> {
            if (k.contains(PREFIX)) {
                finalMap.put(k.replace(PREFIX, ""), v);
            }
        });
        return finalMap;
    }


}
