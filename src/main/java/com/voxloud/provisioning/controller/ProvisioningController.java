package com.voxloud.provisioning.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.voxloud.provisioning.exceptions.DeviceNotFoundException;
import com.voxloud.provisioning.service.ProvisioningService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Slf4j
public class ProvisioningController {

    private final ProvisioningService provisioningService;

    public ProvisioningController(ProvisioningService provisioningService) {
        this.provisioningService = provisioningService;
    }

    @GetMapping("/{mac-address}")
    public ResponseEntity<Map<String, String>> getConfigForDevice(
            @PathVariable("mac-address") String macAddress) throws JsonProcessingException {
        log.info("Incoming request for Mac Address: {}.", macAddress);

        Map<String, String> provisioningParams = provisioningService.getProvisioningFile(macAddress);
        if(provisioningParams.isEmpty()) {
            log.error("FAILURE: Mac Address NOT FOUND: {}.", macAddress);
            throw new DeviceNotFoundException();
        }

        log.info("SUCCESS: Returning Provision params for Mac Address: {}.", macAddress);
        return ResponseEntity.ok().body(provisioningParams);
    }
}