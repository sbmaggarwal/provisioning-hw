package com.voxloud.provisioning.converter;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DeskConverter implements DeviceConverter {

    @Override
    public Map<String, String> updateFromFragment(Map<String, String> map, String fragment) {
        String[] parsedFragment = fragment.split("\\n");
        for (String pair : parsedFragment) {
            String[] parsedPair = pair.split("=");
            map.put(parsedPair[0], parsedPair[1]);
        }
        return map;
    }

    @Override
    public String serialize(Map<String, String> map) {
        return map.keySet().stream()
                .map(key -> key + "=" + map.get(key))
                .collect(Collectors.joining("\n"));
    }
}
