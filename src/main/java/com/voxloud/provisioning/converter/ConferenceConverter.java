package com.voxloud.provisioning.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class ConferenceConverter implements DeviceConverter {

    private final ObjectMapper objectMapper;

    public ConferenceConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Map<String, String> updateFromFragment(Map<String, String> map, String fragment) {
        try {
            TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
            Map<String, String> parsedFragment = objectMapper.readValue(fragment, typeRef);
            parsedFragment.forEach(map::put);
        } catch (JsonProcessingException e) {
            log.error("Can't parse json fragment" + fragment, e);
        }
        return map;
    }

    @Override
    public String serialize(Map<String, String> map) {
        return null;
    }
}
