package com.voxloud.provisioning.converter;

import java.util.Map;

public interface DeviceConverter {
    Map<String, String> updateFromFragment(Map<String, String> map, String fragment);
    String serialize(Map<String, String> map);
}
